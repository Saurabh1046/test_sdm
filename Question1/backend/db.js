const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'demodatabase',
    user: 'root',
    password: 'root',
    database: 'demodb',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})

module.exports = pool

