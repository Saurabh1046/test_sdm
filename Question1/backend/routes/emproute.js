const { response } = require('express');
const express = require('express');
const db = require('../db')
const utils = require('../utils')

const route = express.Router();



route.get('/', (request, response) => {


    const query = `SELECT * FROM emp`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})


route.post('/', (request, response) => {
    const { name, salary, age } = request.body;

    const query = `INSERT INTO emp
                     (name,salary,age) 
                     VALUES 
                     ('${name}',${salary}, ${age})`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})



route.put('/:id', (request, response) => {
    const { id } = request.params;
    const { name, salary, age } = request.body;

    const query = `UPDATE emp
                     SET
                        name ='${name}',
                        salary =${salary},
                        age = ${age}
                     WHERE
                        empid =${id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})


route.delete('/:id', (request, response) => {
    const { id } = request.params;


    const query = `DELETE FROM emp
                    WHERE
                     empid =${id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})

route.get('/:id', (request, response) => {
    const { id } = request.params;


    const query = `SELECT * FROM emp
                    WHERE
                     empid =${id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})







module.exports = route
