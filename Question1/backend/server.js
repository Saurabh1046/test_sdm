const express = require('express')
const cors = require('cors')
const emproute = require('./routes/emproute')

const app = express();
app.use(express.json());
app.use(cors())

app.use('/emp', emproute);

app.listen(4000, '0.0.0.0', () => {
    console.log("The App is listening on port number 4000");
})