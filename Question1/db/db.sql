create database demodb;
use demodb;

create table emp(
    empid INT PRIMARY KEY AUTO_INCREMENT,
    name varchar(50),
    salary FLOAT,
    age INT
);


INSERT INTO emp (name,salary,age) VALUES ('saurabh',110000,23);

